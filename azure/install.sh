#!/bin/bash
sudo snap install docker

is_bombarding=False
is_connected_to_vpn=False

set -m
sudo docker-compose up --build | while read -r p ; 
do
 [[ $is_bombarding == False && "$p" == *"Bombarding"* ]] && is_bombarding=True
 [[ $is_connected_to_vpn == False && "$p" == *"Initialization Sequence Completed"* ]] && is_connected_to_vpn=True

 if [[ $is_bombarding == True && $is_connected_to_vpn == True ]]; then
    echo "Installed and healty"
    sh -c 'PGID=$( sudo ps -o pgid= $$ | tr -d \  ); sudo kill -TERM -$PGID'
 fi 
done &
wait
