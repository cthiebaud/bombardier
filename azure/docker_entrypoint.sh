#!/bin/sh
curl --output resources_all.txt --connect-timeout 1 -m 3 -fsSL $RESOURCES_URL

[ ! -f  resources_all.txt ] && return 1

while IFS="" read -r p || [ -n "$p" ]
do
  bombardier -d ${INTERVAL} -c ${CONNECTIONS} -l $p &
done < resources_all.txt

sleep ${INTERVAL}
